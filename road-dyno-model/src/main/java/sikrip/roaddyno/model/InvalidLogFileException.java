package sikrip.roaddyno.model;


public class InvalidLogFileException extends Exception {

	public InvalidLogFileException(String message) {
		super(message);
	}
}
