package sikrip.roaddyno.engine;

/**
 * Thrown when the simulator cannot be completed.
 */
public class SimulationException extends Exception {

	public SimulationException(String message) {
		super(message);
	}
}
